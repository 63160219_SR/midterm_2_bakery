/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

import java.util.Scanner;

/**
 *
 * @author sairu
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner wr = new Scanner(System.in);

        int totalprice = 0;
        Manu manu = new Manu();
        Bake bake = new Bake();
        cheeseCake cheesecake = new cheeseCake(10, 75, manu, bake);
        chocolateCake chocolatecake = new chocolateCake(10, 45, manu, bake);
        honeyToast honeytoast = new honeyToast(10, 60, manu, bake);
        panCake pancake = new panCake(10, 20, manu, bake);
        strawberryCake strawberrycake = new strawberryCake(10, 45, manu, bake);
        manu.sethoneyToast(honeytoast);
        manu.setcheeseCake(cheesecake);
        manu.setchocolateCake(chocolatecake);
        manu.setpanCake(pancake);
        manu.setstrawberryCake(strawberrycake);
        manu.showManu();
        System.out.println("your order");
        while (true) {
            // 1|2|3|4|5|q: quit
            System.out.println("The menu number you want to select.");
            char numberManu = wr.next().charAt(0);
            if (numberManu == 'q') {
                System.out.println("Thank you. Have a delicious meal.");
                break;
            } else if (numberManu == '1') {
                System.out.println("honeytoast : how many pieces");
                int amount = wr.nextInt();
                if (amount > honeytoast.amount) {
                    System.out.println("not enough bakery");
                } else {
                    honeytoast.amount = honeytoast.amount - amount;
                    totalprice += honeytoast.price * amount;
                }
            } else if (numberManu == '2') {
                System.out.println("cheesecake : how many pieces");
                int amount = wr.nextInt();
                if (amount > cheesecake.amount) {
                    System.out.println("not enough bakery");
                } else {
                    cheesecake.amount = cheesecake.amount - amount;
                    totalprice += cheesecake.price * amount;
                }

            } else if (numberManu == '3') {
                System.out.println("chocolatecake : how many pieces");
                int amount = wr.nextInt();
                if (amount > chocolatecake.amount) {
                    System.out.println("not enough bakery");
                } else {
                    chocolatecake.amount = chocolatecake.amount - amount;
                    totalprice += chocolatecake.price * amount;
                }
            } else if (numberManu == '4') {
                System.out.println("pancake : how many pieces");
                int amount = wr.nextInt();
                if (amount > pancake.amount) {
                    System.out.println("not enough bakery");
                } else {
                    pancake.amount = pancake.amount - amount;
                    totalprice += pancake.price * amount;
                }
            } else if (numberManu == '5') {
                System.out.println("strawberrycake : how many pieces");
                int amount = wr.nextInt();
                if (amount > strawberrycake.amount) {
                    System.out.println("not enough bakery");
                } else {
                    strawberrycake.amount = strawberrycake.amount - amount;
                    totalprice += strawberrycake.price * amount;
                }
            }
        }
        manu.showManu();
        bake.setcheeseCake(cheesecake);
        bake.setchocolateCake(chocolatecake);
        bake.sethoneyToast(honeytoast);
        bake.setpanCake(pancake);
        bake.setstrawberryCake(strawberrycake);
        bake.bake('1');
        bake.bake('3', 2);
        bake.bake();
        bake.bake(4);
        manu.showManu();
        System.out.println(totalprice + " Baht");
    }
}
