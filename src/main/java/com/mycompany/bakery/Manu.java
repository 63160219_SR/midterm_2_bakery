/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

/**
 *
 * @author sairu
 */
public class Manu {

    private cheeseCake cheesecake;
    private chocolateCake chocolatecake;
    private honeyToast honeytoast;
    private panCake pancake;
    private strawberryCake strawberrycake;
    private Bakery[] bakerys = new Bakery[10];
    int BakeryCount = 0;

    public Manu() {

    }

    public void addBakery(Bakery bakery) {
        bakerys[BakeryCount] = bakery;
        System.out.println(bakerys[BakeryCount].name + " Added");
        BakeryCount++;
    }

    public void setcheeseCake(cheeseCake cheesecake) {
        this.cheesecake = cheesecake;
        addBakery(cheesecake);
    }

    public void setchocolateCake(chocolateCake chocolatecake) {
        this.chocolatecake = chocolatecake;
        addBakery(chocolatecake);
    }

    public void sethoneyToast(honeyToast honeytoast) {
        this.honeytoast = honeytoast;
        addBakery(honeytoast);
    }

    public void setpanCake(panCake pancake) {
        this.pancake = pancake;
        addBakery(pancake);
    }

    public void setstrawberryCake(strawberryCake strawberrycake) {
        this.strawberrycake = strawberrycake;
        addBakery(strawberrycake);
    }

    public void showManu() {
        showTitle();
        System.out.println(honeytoast);
        System.out.println(cheesecake);
        System.out.println(chocolatecake);
        System.out.println(pancake);
        System.out.println(strawberrycake);
        System.out.println("");

    }

    private void showTitle() {
        System.out.println("");
        System.out.println("Manu");
    }

}
