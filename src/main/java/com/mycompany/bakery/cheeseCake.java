/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

/**
 *
 * @author sairu
 */
public class cheeseCake extends Bakery{
    private Manu manu;
    private Bake bake;
    
    public cheeseCake(int amount,int price,Manu manu,Bake bake){
        super("cheesecake",amount,price);
        this.manu = manu;
        this.bake = bake;
    }
    @Override
    public String toString() {
        return "2 cheesecake: "+price+" Baht "+amount+" piece";
    }
    
}
