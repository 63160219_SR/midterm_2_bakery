/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

/**
 *
 * @author sairu
 */
public class Bake {

    private char lastnumberManu = ' ';
    private cheeseCake cheesecake;
    private chocolateCake chocolatecake;
    private honeyToast honeytoast;
    private panCake pancake;
    private strawberryCake strawberrycake;

    public Bake() {

    }

    public void setcheeseCake(cheeseCake cheesecake) {
        this.cheesecake = cheesecake;

    }

    public void setchocolateCake(chocolateCake chocolatecake) {
        this.chocolatecake = chocolatecake;

    }

    public void sethoneyToast(honeyToast honeytoast) {
        this.honeytoast = honeytoast;

    }

    public void setpanCake(panCake pancake) {
        this.pancake = pancake;

    }

    public void setstrawberryCake(strawberryCake strawberrycake) {
        this.strawberrycake = strawberrycake;

    }

    public boolean bake(char number) {
        switch (number) {
            case '1':
                showTitleBake();
                honeytoast.amount++;
                break;
            case '2':
                showTitleBake();
                cheesecake.amount++;
                break;
            case '3':
                showTitleBake();
                chocolatecake.amount++;
                break;
            case '4':
                showTitleBake();
                pancake.amount++;
                break;
            case '5':
                showTitleBake();
                strawberrycake.amount++;
                break;
        }
        lastnumberManu = number;
        return true;
    }

    public boolean bake(char number, int turn) {
        for (int i = 0; i < turn; i++) {
            if (!this.bake(number)) {
                return false;
            }
        }
        showTitleBake();
        return true;
    }

    public boolean bake() {
        return this.bake(lastnumberManu);
    }

    public boolean bake(int turn) {
        return this.bake(lastnumberManu, turn);
    }
    public void showTitleBake(){
        System.out.println("baking now");
    }
}
