/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

/**
 *
 * @author sairu
 */
public abstract class Bakery {
    protected String name;
    protected int amount;
    protected int price;
    

    public Bakery(String name,int amount, int price){
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getamount() {
        return amount;
    }
}
