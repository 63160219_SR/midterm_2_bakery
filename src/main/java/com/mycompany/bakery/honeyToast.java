/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bakery;

/**
 *
 * @author sairu
 */
public class honeyToast extends Bakery{
    private Manu manu;
    private Bake bake;
    
    public honeyToast(int amount,int price,Manu manu,Bake bake){
        super("honeytoast",amount,price);
        this.manu = manu;
        this.bake = bake;
    }
    @Override
    public String toString() {
        return "1 honeytoast: "+price+" Baht "+amount+" piece";
    }
}
